package com.cluster;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

/**
 * Created by shreyas on 16/4/16.
 */
public class Main {
    private JButton server;
    private JButton client;
    private JPanel welcomePanel;
    private JLabel imageLabel;
    private JLabel nodeType;
    private JLabel ipAddress;
    public static boolean serverClicked = false;

    public Main() throws UnknownHostException, SocketException {
        nodeType.setFont(new Font("serif", Font.BOLD, 16));
        imageLabel.setText("");
        ipAddress.setText(Constants.getIpAdress());
        server.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                serverClicked = true;
                InputDialog.dialog("Enter client IpAddresses separated by commas.");
                Constants.frame.dispose();
                ExceptionDialog.dialog("Set Key on lower right corner in next screen before you can proceed!");
                try {
                    Constants.sFrame = new Server();
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                } catch (UnsupportedLookAndFeelException e1) {
                    e1.printStackTrace();
                } catch (InstantiationException e1) {
                    e1.printStackTrace();
                } catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                }
            }
        });
        client.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //InputDialog.dialog("Enter Client port to start listening.");
                Constants.frame.dispose();
                try {
                    Constants.cFrame = new Client();
                } catch (SocketException e1) {
                    e1.printStackTrace();
                }
                Constants.cFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                Constants.cFrame.setSize(1280, 720);
                Constants.cFrame.setLocationRelativeTo(null);
                Constants.cFrame.setVisible(true);
                //Client.clientStart(Constants.ServerPort[0]);
            }
        });
    }

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException, UnknownHostException, SocketException {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        Constants.frame = new JFrame("Welcome to Data Encryption");
        Constants.frame.setContentPane(new Main().welcomePanel);
        Constants.frame.setResizable(false);
        Constants.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Constants.frame.setSize(600, 300);
        Constants.frame.setLocationRelativeTo(null);
        Constants.frame.setVisible(true);
    }
}

