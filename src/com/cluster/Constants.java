package com.cluster;

import javax.swing.*;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * Created by shreyas on 16/4/16.
 */
public class Constants {
    public static InetAddress ServerPort[];
    public static JFrame frame = null;
    public static JFrame sFrame = null;
    public static JFrame cFrame = null;
    public static int ClientPort=4445;
    public static String keyDefault = "Bar12345Bar12345";
    public static String key = "";
    public static String initVector = "RandomInitVector";
    public static String rMsg = "";
    public static String fName = "";

    public static String getIpAdress() throws SocketException {
        Enumeration e = NetworkInterface.getNetworkInterfaces();
        while(e.hasMoreElements())
        {
            NetworkInterface n = (NetworkInterface) e.nextElement();
            Enumeration ee = n.getInetAddresses();
            while (ee.hasMoreElements())
            {
                InetAddress i = (InetAddress) ee.nextElement();
                if(i.getHostAddress().toString().contains("192"))
                    return i.getHostAddress();
            }
        }
        return null;
    }
}
