package com.cluster;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * Created by shreyas on 16/4/16.
 */
public class Client extends JFrame {

    private static Socket socket;
    private JTextArea textArea;

    private JButton buttonStart = new JButton("Start Client");
    private JButton buttonClear = new JButton("Clear");
    private JButton buttonStop = new JButton("Stop and Exit");
    private JLabel ipAddress = new JLabel("IP Adresss : " + Constants.getIpAdress());


    private PrintStream standardOut;

    public Client() throws SocketException {
        OutputTerminal(Constants.ClientPort);
    }


    public void OutputTerminal(int port) {

        textArea = new JTextArea(50, 10);
        textArea.setEditable(false);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        PrintStream printStream = new PrintStream(new CustomOutputStream(textArea));

        // keeps reference of standard output stream
        standardOut = System.out;

        // re-assigns standard output stream and error output stream
        System.setOut(printStream);
        System.setErr(printStream);

        // creates the GUI
        setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(10, 10, 10, 10);
        constraints.anchor = GridBagConstraints.WEST;

        add(buttonStart, constraints);
        add(buttonStop, constraints);
        buttonStop.setVisible(false);
        
        ipAddress.setFont(new Font("serif", Font.BOLD, 16));
        add(ipAddress);

        constraints.gridx = 1;
        constraints.anchor = GridBagConstraints.EAST;
        add(buttonClear, constraints);

        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;

        add(new JScrollPane(textArea), constraints);

        // adds event handler for button Start
        buttonStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                buttonStart.setVisible(false);
                buttonStop.setVisible(true);
                try {
                    startClient(port);
                } catch (IllegalArgumentException e) {
                    ExceptionDialog.dialog("Client port out of range.  Application will now terminate!");
                    System.exit(0);
                }
            }
        });
        buttonStop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                Constants.cFrame.dispose();
                System.exit(0);
            }
        });

        // adds event handler for button Clear
        buttonClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                // clears the text area
                try {
                    textArea.getDocument().remove(0,
                            textArea.getDocument().getLength());
                    standardOut.println("Text area cleared");
                } catch (BadLocationException ex) {
                    ex.printStackTrace();
                }
            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(480, 320);
        setLocationRelativeTo(null);    // centers on screen
    }

    /**
     * Prints log statements for testing in a thread
     */
    private void startClient(int port) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        ServerSocket serverSocket = new ServerSocket(port);
                        System.out.println("-------------------------------Client Started and listening to the port : " + port + " -------------------------------\n");

                        //Client is running always. This is done using this while(true) loop
                        while (true) {
                            //Reading the message from the server
                            socket = serverSocket.accept();
                            InputStream is = socket.getInputStream();
                            InputStreamReader isr = new InputStreamReader(is);
                            BufferedReader br = new BufferedReader(isr);
                            String data = br.readLine();
                            String action[] = data.split("<action>");
                            String size[] = action[1].split("<size>");
                            int sizeValue = Integer.parseInt(size[0]);
                            data = new String(size[1]);
                            System.out.println("-------------------------------Message received from server is : -------------------------------\n" + data + "\n");
                            System.out.println("-------------------------------The action to be performed is : -------------------------------\n" + action[0] + "\n");


                            String returnMessage = null;
                            try {
                                String returnValue = null;
                                if ("encrypt".equalsIgnoreCase(action[0])) {
                                    returnValue = AesCipher.encrypt(Constants.key, Constants.initVector, data);
                                } else if ("decrypt".equalsIgnoreCase(action[0])) {
                                    returnValue = AesCipher.decrypt(Constants.key, Constants.initVector, data);
                                } else if ("keyset".equalsIgnoreCase(action[0])) {
                                    Constants.key = data;
                                    returnValue = "Key is set!!";
                                }
                                returnMessage = new String("<size>" + sizeValue + "</size>" + returnValue + "\n");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            //Sending the response back to the server.
                            OutputStream os = socket.getOutputStream(
                            );
                            OutputStreamWriter osw = new OutputStreamWriter(os);
                            BufferedWriter bw = new BufferedWriter(osw);
                            bw.write(returnMessage);
                            System.out.println("-------------------------------Message sent to the server is : -------------------------------\n" + returnMessage + "\n");
                            bw.flush();
                        }
                    } catch (BindException e) {
                        ExceptionDialog.dialog("Client port already in use.  Application will now terminate!");
                        System.exit(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (IllegalArgumentException e) {
                        ExceptionDialog.dialog("Client port out of range.  Application will now terminate!");
                        System.exit(0);
                    } finally
                    {
                        try {
                            socket.close();
                        } catch (Exception e) {
                        }
                    }
                }
            }
        });
        thread.start();
    }
}