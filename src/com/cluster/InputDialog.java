package com.cluster;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;

public class InputDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTextField inputField;
    private JLabel message;
    private JButton closeButton;


    public InputDialog(String msg) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        message.setText(msg);
        message.setFont(new Font("serif", Font.BOLD, 16));
        final Toolkit toolkit = Toolkit.getDefaultToolkit();
        final Dimension screenSize = toolkit.getScreenSize();
        final int x = (screenSize.width) / 3;
        final int y = (screenSize.height) / 4;
        this.setLocation(x, y);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.setUndecorated(true);

        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }

    private void onOK() {
        dispose();
        if (Main.serverClicked) {
            try {
                String input = inputField.getText();
                String ports[] = input.split(",");
                ArrayList <InetAddress> address = new ArrayList<>();
                for(String e : ports){
                    System.out.println(e);
                address.add(InetAddress.getByName(e));
                }
                Constants.ServerPort=address.toArray(new InetAddress[address.size()]);
                System.out.println(Arrays.toString(Constants.ServerPort));
            } catch (Exception e) {
                e.printStackTrace();
                ExceptionDialog.dialog("Input not valid! Application will now terminate");
                System.exit(0);
            }
            Main.serverClicked = false;
        }
    }


    public static void dialog(String msg) {
        InputDialog dialog = new InputDialog(msg);
        dialog.pack();
        dialog.setVisible(true);
    }
}
