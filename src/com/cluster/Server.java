package com.cluster;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;


/**
 * Created by shreyas on 16/4/16.
 */
public class Server extends JFrame {
    private static Socket socket;
    private JPanel serverPanel;
    private JTextArea inputData;
    private JButton encrypt;
    private JButton Decrypt;
    private JButton clear;
    private JButton browse;
    private JScrollPane scrollPane;
    private JLabel fileLabel;
    private JLabel textLabel;
    private JButton setKey;
    private JLabel KeyLabel;
    private JLabel statusValue;
    private JLabel disconnectLabel;
    private JLabel connectedLabel;
    public static boolean firstMessage = true;
    public static boolean decryptFlag = false;
    public static boolean fileChoosen = false;
    public static boolean setKeyFlag = false;

    public Server() throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        this.connectedLabel.setText("");
        this.connectedLabel.setVisible(false);
        this.setTitle("Server Page");
        this.setContentPane(this.serverPanel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1280, 720);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.encrypt.setEnabled(false);
        this.Decrypt.setEnabled(false);
        this.browse.setEnabled(false);
        this.clear.setEnabled(false);
        this.inputData.setLineWrap(true);
        this.inputData.setWrapStyleWord(true);
        this.fileLabel.setFont(new Font("serif", Font.BOLD, 16));
        this.textLabel.setFont(new Font("serif", Font.BOLD, 16));
        this.KeyLabel.setFont(new Font("serif", Font.BOLD, 16));
        this.scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        this.scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        encrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setKeyFlag = false;
                String input = null;
                if (!fileChoosen) {
                    input = inputData.getText();
                } else {
                    inputData.setEditable(false);
                    try {
                        input = readFile(Constants.fName, Charset.defaultCharset());
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                String data = input.replaceAll("[\\t\\n\\r]+", " ");
                int size = data.length() / Constants.ServerPort.length;
                if (data.length() % Constants.ServerPort.length != 0) {
                    size++;
                }
                String dataSplit[] = data.split("(?<=\\G.{" + size + "})");
                //Constants.sFrame.dispose();
                for (int i = 0; i < Constants.ServerPort.length; i++) {
                    serverStart(Constants.ServerPort[i], dataSplit[i], "encrypt", Constants.ServerPort.length);
                }
            }
        });
        Decrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setKeyFlag = false;
                decryptFlag = true;
                String input = null;
                if (!fileChoosen) {
                    input = inputData.getText();
                } else {
                    inputData.setEditable(false);
                    try {
                        input = readFile(Constants.fName, Charset.defaultCharset());
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                String data = input.replaceAll("[\\t\\n\\r]+", " ");
                String sizeTemp[] = data.split("</size>");
                String sizeValue[] = sizeTemp[0].split("<size>");
                data = new String(sizeTemp[1]);
                String dataSplit[] = data.split("</mEnd>");
                for (int i = 0; i < Integer.parseInt(sizeValue[1]); i++) {
                    serverStart(Constants.ServerPort[(i % Constants.ServerPort.length)], dataSplit[i], "decrypt", 0);// round robin decrypting
                }
            }
        });
        clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    inputData.getDocument().remove(0, inputData.getDocument().getLength());
                    System.out.println("Text area cleared");
                    Constants.rMsg = "";
                    firstMessage = true;
                    decryptFlag = false;
                    fileChoosen = false;
                    inputData.setEditable(true);
                } catch (BadLocationException ex) {
                    ex.printStackTrace();
                }
            }
        });
        browse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                inputData.setEditable(false);
                inputData.setText("");
                JFileChooser chooser = new JFileChooser();
                chooser.showOpenDialog(null);
                File f = chooser.getSelectedFile();
                try {
                    String filename = f.getAbsolutePath();
                    Constants.fName = filename;
                    inputData.setText("-------------------------------The chosen file is : " + Constants.fName + "-------------------------------\n");
                    fileChoosen = true;
                } catch (Exception exception) {
                    ExceptionDialog.dialog("File not choosen!");
                    inputData.setEditable(true);
                    exception.printStackTrace();
                }
            }
        });
        setKey.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String input = null;
                setKeyFlag = true;
                input = inputData.getText();
                String data = input.replaceAll("[\\t\\n\\r]+", " ");
                Constants.rMsg = "";
                if (data.length() != 16) {
                    ExceptionDialog.dialog("Enter Key of correct length(Excluding spaces)!! NOTE: Default key is set ");
                    data = Constants.keyDefault;
                }
                //Constants.sFrame.dispose();
                for (int i = 0; i < Constants.ServerPort.length; i++) {
                    serverStart(Constants.ServerPort[i], data, "keyset", Constants.ServerPort.length);
                    encrypt.setEnabled(true);
                    Decrypt.setEnabled(true);
                    browse.setEnabled(true);
                    clear.setEnabled(true);
                    Constants.rMsg = "";
                    firstMessage = true;
                }
                statusValue.setText("Connected!");
                disconnectLabel.setVisible(false);
                connectedLabel.setVisible(true);
                connectedLabel.setText("IpAddresses : "+ Arrays.toString(Constants.ServerPort));
            }
        });
    }

    public void serverStart(InetAddress ipAddress, String data, String action, int size) {

        try {
            inputData.setEditable(true);
            try {
                socket = new Socket(ipAddress, Constants.ClientPort);
            } catch (Exception e) {
                ExceptionDialog.dialog("Invalid Ip Address!  Application will now terminate!");
                this.dispose();
                e.printStackTrace();
                System.exit(0);
            }

            //Send the message to the client
            OutputStream os = socket.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            String sendMessage = action + "<action>" + size + "<size>" + data + "\n";
            bw.write(sendMessage);
            bw.flush();
            //System.out.println("Message sent to the client : " + sendMessage);
            inputData.append("\n\n-------------------------------Message sent to the client : -------------------------------\n" + sendMessage + "\n");
            //Get the return message from the client
            InputStream is = socket.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String message = br.readLine();
            if (firstMessage && !decryptFlag) {
                Constants.rMsg = Constants.rMsg + message + "</mEnd>";
                firstMessage = false;
            } else if (!decryptFlag) {
                String temp[] = message.split("</size>");
                Constants.rMsg = Constants.rMsg + temp[1] + "</mEnd>";
            } else {
                String temp[] = message.split("</size>");
                Constants.rMsg = Constants.rMsg + temp[1];
            }
            //System.out.println("Message received from the client : " + message);
            inputData.append("-------------------------------Message received from the client : -------------------------------\n" + message + "\n");
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            //Closing the socket
            try {
                //System.out.println("The received message is : " + Constants.rMsg);
                if (!Server.setKeyFlag) {
                    inputData.append("\n-------------------------------The received message is : -------------------------------\n" + Constants.rMsg + "\n");
                    inputData.append("\n-------------------------------The encryption/decryption process is done-------------------------------" + "\n");
                }
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    static String readFile(String path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}

