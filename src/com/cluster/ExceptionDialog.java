package com.cluster;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ExceptionDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JLabel message;


    public ExceptionDialog(String msg) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        message.setText(msg);
        //this.setLocationRelativeTo(null);
        final Toolkit toolkit = Toolkit.getDefaultToolkit();
        final Dimension screenSize = toolkit.getScreenSize();
        final int x = (screenSize.width) / 3;
        final int y = (screenSize.height) / 4;
        this.setLocation(x, y);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
// add your code here
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    public static void dialog(String msg) {
        ExceptionDialog dialog = new ExceptionDialog(msg);
        dialog.pack();
        dialog.setVisible(true);
    }
}
